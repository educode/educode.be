_model: atelier
---
title: Enseigner en ligne
---
description:

Dans cette formation, nous allons aborder la difficile question de l'enseignement en ligne, à laquelle de nombreux enseignants de primaire, secondaire, hautes-écoles et universités dans le monde entier ont été confrontés depuis le 13 mars 2020 à l'occasion du confinement obligatoire imposé par le [Maladie à coronavirus 2019](https://fr.wikipedia.org/wiki/Maladie_%C3%A0_coronavirus_2019).

Nous aborderons notamment les points suivants :
- bref historique de l'enseignement à distance
- ses contraintes et quelques pistes de moyens et méthodes, en mode synchrone comme asynchrone,
- quelques outils
  - hors ligne, asynchrones
  - en ligne, synchrones et asynchrones tels que
     - texte (mail, pad, site web, ...)
     - son (téléphone et autres)
     - vidéo (outils de vidéoconférence généralistes ou spécialisés)
     - système de gestion de contenu pédagogique
     - outils de quiz et question en ligne
- comment organiser des évaluations, formatives et certificatives
---
projet: EducodeSchool
---
schedule:

* Mardi 12 mai de 10h à 11h (cours 1, gratuit, [don attendu](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 19 mai de 10h à 11h (cours 2, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 2 juin de 10h à 11h (cours 3, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 9 juin de 15h à 16h (cours 4, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil))
---
description_long:

Dans cette formation, nous allons aborder la difficile question de l'enseignement en ligne, à laquelle de nombreux enseignants de primaire, secondaire, hautes-écoles et universités dans le monde entier ont été confrontés depuis le 13 mars 2020 à l'occasion du confinement obligatoire imposé par le [Maladie à coronavirus 2019](https://fr.wikipedia.org/wiki/Maladie_%C3%A0_coronavirus_2019).

Nous aborderons notamment les points suivants :
- bref historique de l'enseignement à distance
- ses contraintes et quelques pistes de moyens et méthodes, en mode synchrone comme asynchrone,
- quelques outils
  - hors ligne, asynchrones
  - en ligne, synchrones et asynchrones tels que
     - texte (mail, pad, site web, ...)
     - son (téléphone et autres)
     - vidéo (outils de vidéoconférence généralistes ou spécialisés)
     - système de gestion de contenu pédagogique
     - outils de quiz et question en ligne
- comment organiser des évaluations, formatives et certificatives

Les cours sont accessibles aux personnes inscrites dans ce [formulaire](https://framaforms.org/educodeschool-inscription-aux-formations-en-ligne-1588750555), qui sont [membres de l'association](https://opencollective.com/educode) (20 € par an) et qui ont [payé les cours](https://opencollective.com/educode) (10€ par cours, payable avant le cours, cours par cours - 35 € pour les 4 cours). Les cours sont interactifs et ouverts à **maximum 20 personnes** (sauf le premier, qui sera ouvert à autant de personnes qui le désirent, mais sera moins interactif). Ceci signifie que les participants pourront poser des questions, intervenir, soumettre les problématiques …

Organisé dans le salon [EducodeTV](https://bbb-1.wsweet.cloud/b/nic-7vu-ygq) (qui ouvrira 10 minutes avant le cours).
