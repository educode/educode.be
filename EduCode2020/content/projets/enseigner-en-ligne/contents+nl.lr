_model: atelier
---
title: Leer online
---
description:

In deze opleiding gaan we in op de moeilijke kwestie van het online onderwijs, waarmee veel docenten uit het basis-, middelbaar, middelbaar en universitair onderwijs wereldwijd sinds 13 maart 2020 worden geconfronteerd in het kader van de verplichte inperking die door [Coronavirus 2019 Disease](https://fr.wikipedia.org/wiki/Maladie_%C3%A0_coronavirus_2019) wordt opgelegd.

We zullen met name de volgende punten aan de orde stellen:
- een korte geschiedenis van afstandsonderwijs
- zijn beperkingen en enkele sporen van middelen en methoden, zowel in synchrone als in asynchrone modus,
- enkele hulpmiddelen
  - offline, asynchroon
  - in-line, synchroon en asynchroon zoals
     - tekst (mail, pad, website, ...)
     - son (téléphone et autres)
     - video (algemene of gespecialiseerde videoconferentie-instrumenten)
     - leerinhoudsbeheersysteem
     - online quiz tools en vragen
- hoe evaluaties, opleidingen en certificeringen te organiseren
---
projet: EducodeSchool
---
schedule:

* Mardi 12 mai de 10h à 11h (cours 1, gratuit, [don attendu](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 19 mai de 10h à 11h (cours 2, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 2 juin de 10h à 11h (cours 3, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil)),
* Mardi 9 juin de 15h à 16h (cours 4, [payant](https://wiki.educode.be/doku.php/formations/modalites_pratiques/accueil))
---
description_long:

In deze opleiding gaan we in op de moeilijke kwestie van het online onderwijs, waarmee veel docenten uit het basis-, middelbaar, middelbaar en universitair onderwijs wereldwijd sinds 13 maart 2020 worden geconfronteerd in het kader van de verplichte inperking die door [Coronavirus 2019 Disease](https://fr.wikipedia.org/wiki/Maladie_%C3%A0_coronavirus_2019) wordt opgelegd.

We zullen met name de volgende punten aan de orde stellen:
- een korte geschiedenis van afstandsonderwijs
- zijn beperkingen en enkele sporen van middelen en methoden, zowel in synchrone als in asynchrone modus,
- enkele hulpmiddelen
  - offline, asynchroon
  - in-line, synchroon en asynchroon zoals
     - tekst (mail, pad, website, ...)
     - son (téléphone et autres)
     - video (algemene of gespecialiseerde videoconferentie-instrumenten)
     - leerinhoudsbeheersysteem
     - online quiz tools en vragen
- hoe evaluaties, opleidingen en certificeringen te organiseren

Les cours sont accessibles aux personnes inscrites dans ce [formulaire](https://framaforms.org/educodeschool-inscription-aux-formations-en-ligne-1588750555), qui sont [membres de l'association](https://opencollective.com/educode) (20 € par an) et qui ont [payé les cours](https://opencollective.com/educode) (10€ par cours, payable avant le cours, cours par cours - 35 € pour les 4 cours). Les cours sont interactifs et ouverts à **maximum 20 personnes** (sauf le premier, qui sera ouvert à autant de personnes qui le désirent, mais sera moins interactif). Ceci signifie que les participants pourront poser des questions, intervenir, soumettre les problématiques …

Organisé dans le salon [EducodeTV](https://bbb-1.wsweet.cloud/b/nic-7vu-ygq) (qui ouvrira 10 minutes avant le cours).
