_model: atelier
---
title: Quelle école (numérique ?) pour la rentrée de septembre 2020 ?
---
description: Dire que l'année scolaire 2019-2020 a été bousculée est pour nous tous un doux euphémisme. Enseignants, élèves, parents, nous avons tous été bousculés dans nos pratiques et nos habitudes. Avec précipitation souvent, impréparation, manque de formation autant que de soutien, les enseignants ont été précipités « spécialistes de l'enseignement à distance avec le numérique (ou pas) ». Mais maintenant, avec un peu de recul et d'expériences, que voulons nous comme rentrée scolaire ? Quelle école sera la nôtre à la prochaine rentrée ? Comme avant avec beaucoup de présentiel et un peu de numérique ou une école mixte, avec du numérique pour l'enseignement à distance et du présentiel ? Qu'est-ce qui a marché ou pas ? Qu'est-ce qui répond à nos attentes et à celles des élèves et de leurs parents ? Comment concilier attentes, moyens, outils, bien être, éthique, respect et efficacité ? De vraies questions complexes.
---
description_long:

### Thème et discussion

Dire que l'année scolaire 2019-2020 a été bousculée est pour nous tous un doux euphémisme. Enseignants, élèves, parents, nous avons tous été bousculés dans nos pratiques et nos habitudes. Avec précipitation souvent, impréparation, manque de formation autant que de soutien, les enseignants ont été précipités « spécialistes de l'enseignement à distance avec le numérique (ou pas) ». Mais maintenant, avec un peu de recul et d'expériences, que voulons nous comme rentrée scolaire ? Quelle école sera la nôtre à la prochaine rentrée ? Comme avant avec beaucoup de présentiel et un peu de numérique ou une école mixte, avec du numérique pour l'enseignement à distance et du présentiel ? Qu'est-ce qui a marché ou pas ? Qu'est-ce qui répond à nos attentes et à celles des élèves et de leurs parents ? Comment concilier attentes, moyens, outils, bien être, éthique, respect et efficacité ? De vraies questions complexes.

L'idée des rencontres en ligne de ces mercredi 17 juin & 1er juillet 2020 de 14h à 16h UTC+2 est que des acteurs du terrain disent publiquement devant d'autres autant que des responsables ce qu'ils ont vécu et ce qu'ils souhaitent. Pour que les décisions ne soient pas prises sans qu'elles et ils aient eu le droit à la parole, nécessaire et jamais suffisant.

Et que des perspectives internationales (au moins d'Argentine, de Belgique, France, Québec et Suisse) soient échangées pour ouvrir les discussions et les esprits.

La conférence est organisée sur 2 jours :
1. le mercredi 17 juin de 14 à 16h UTC+2, pour aborder le fondamental (maternelle & primaire) et le début du secondaire (3 - 14 ans)
2. le mercredi 1er juillet de 14 à 16h UTC+2, pour aborder la fin du secondaire et le supérieur (15 - 25 ans)

Ceci pour ne pas être trop longue par session et distinguer 2 niveaux qui partagent sûrement des éléments mais sont quand même fort différents.

### Inscriptions

Les inscriptions aux conférences Educode 2020 seront ouvertes très bientôt. Tout se passera en ligne, comme un très grand nombre d'activités en cette période de fin de confinement.

**L'inscription est obligatoire** et la participation est **presque gratuite** : chacun donne ce qu'il veut ou peut, une somme supérieure à 1 €. Autrement dit, un **don est attendu**.

Pour s'inscrire, il suffit de :

* pour tous : aller sur la page [inscriptions.educode.be](https://inscriptions.educode.be) pour prendre votre ticket.
* pour les enseignants IFC1) : il est aussi nécessaire, pour avoir la gratuité, de s'inscrire après sur [IFC](http://www.ifc.cfwb.be/) avec le numéro de formation XXXXXXXXXX (à recevoir très bientôt). Merci d'utiliser le même email que pour l'inscription précédente.

### Programme

L'ordre des exposés est encore à déterminer ainsi que une éventuelle sélection de ceux-ci.

Les exposés dureront de 5 à 15 minutes pour laisser du temps à la discussion. Pour le moment, les orateurs qui ont répondu en marquant de l’intérêt sont repris ci-dessous par ordre alphabétique. Nous voulons obtenir la parité homme - femme pour les oratrices et orateurs.

1. exposé de Olivier Berger
1. exposé de [Bibiana Boccolini](/intervenants/bibiana-boccolini/) (f)
1. exposé de [Laurence Bourguignon](/intervenants/laurence-bourguignon/) (f)
1. exposé de [Bruno De Lièvre](https://staff.umons.ac.be/bruno.delievre/pubsfr.html)
1. exposé de [Jacques Folon](https://www.folon.com/) (Belgique)
1. exposé de Benjamin Gentils (France)
1. exposé de Sandrine Geuquet (f) (Belgique)
1. exposé de Hugo Herter (Belgique)
1. exposé de [Bruno Humbeeck](https://fr.wikipedia.org/wiki/Bruno%20Humbeeck) (Belgique)
1. exposé de [Georges Khaznadar](/intervenants/georges-khaznadar/) (France)
1. exposé de Fabrice Lenoble (France)
1. exposé de [Lionel Maurel](https://fr.wikipedia.org/wiki/Lionel%20Maurel) (aussi connu sous le nom calimaq) (France)
1. exposé de [François Mottard](/intervenants/francois-mottard/) (Belgique)
1. exposé de [Bernard Rentier](/intervenants/bernard-rentier/)  (Belgique)
1. exposé de Fabian Rodriguez (Québec)
1. exposé de [Margarida Romero](/intervenants/margarida-romero/) (f) (France et Québec)
1. exposé de [François Taddei](https://fr.wikipedia.org/wiki/Fran%C3%A7ois%20Taddei) (France)
1. exposé de [Damien Van Achter](https://davan.ac/) (Belgique)

*Légende : (f) = femme*

### Intéressés mais pas disponibles

1. Vincent Englebert
2. [Philippe Meirieu](https://fr.wikipedia.org/wiki/Philippe%20Meirieu)

### Teasings

1. [Bruno Humbeeck introduit son exposé de la conférence](https://videos.domainepublic.net/videos/embed/3908523d-6dd5-4ce7-ba94-486871ab49fc) (1 minute 50 secondes) et [son texte d'introduction](/intervenants/bruno-humbeeck/)

### Partenaires

1. [Société informatique de France](https://www.societe-informatique-de-france.fr/)
2. [Université libre de Bruxelles](https://wiki.educode.be/doku.php/structures/universite_libre_de_bruxelles)
3. [Université de Liège](https://wiki.educode.be/doku.php/structures/universite_de_liege) (à confirmer)
4. [Université de Namur](https://unamur.be/) (à confirmer)
5. [Université de Mons](https://www.umons.ac.be/) (à confirmer)
6. [Haute-école Bruxelles Brabant](https://wiki.educode.be/doku.php/structures/he2b) (à confirmer)

<div class="col-12 text-center p-4">
                        <a href="https://bbb-1.wsweet.cloud/b/nic-7vu-ygq" class="btn btn-lg btn-warning text-uppercase" target="blank">
                        <i class="fa fa-eye"></i>
                           Rejoindre la conférence</a>
                    </div>
---
projet: EducodeSchool
---
schedule:

1. Mercredi 17 juin 2020 de 14h00 à 16h00 : à propos du primaire et du début secondaire
2. Mercredi 1er juillet 2020 de 14h00 à 16h00 : à propos de la fin du secondaire et du supérieur

La conférence de ce mercredi 17 juin aura lieu dans la salon EducodeTV qui ouvrira 15 minutes avant le début celle-ci (ouverture du salon à 13h45)
