_model: atelier
---
title: Petit aperçu d'informatique sans ordi… dans mon salon
---
description:

Cet atelier, animé par [Marie Duflot-Kremer](/intervenants/marie-duflot-kremer/), regroupe trois webinaires :

**Un peu d'informagie à faire à la maison :**

Nous vous proposons une petite parenthèse mêlant informatique et magie.
Non il n'y a rien de magique dans l'informatique, l'ordinateur n'a pas sa volonté propre et tout n'est pas "trop compliqué".  Nous vous proposons dans ce webinaire de découvrir des concepts informatiques et de les mettre en pratique avec des objets du quotidien, de manière amusante et apparemment magique. Alors venez vous amuser, réfléchir, comprendre et repartez avec quelques tours (informatiques) dans votre sac. Le premier tour de magie est compréhensible et faisable dès 5-6 ans.

**Jouons avec l'intelligence artificielle à la maison :**

Une nouvelle idée d'activité ludique et facilement reproductible avec des enfants à la maison ou ailleurs : parler d'intelligence artificielle et plus particulièrement d'apprentissage… avec des gobelets et des jetons. Venez découvrir comment une machine apparemment toute simple peut apprendre et devenir infaillible à un simple jeu de stratégie, et repartez avec une activité clé en main à reproduire avec vos amis/enfants/élèves.

** Sage comme une image ? **
En informatique toutes les informations sont codées en binaire, même les images. Dans cette activité, vous découvrirez comment on peut se transmettre une image et reconstituerez vous-mêmes une image à partir d'un codage qui permet de les compresser.

Indication : se munir de papier quadrillé et d'un crayon ou stylo noir.
---
projet: EducodeSchool
---
schedule:

- Ce jeudi 14 mai 2020 de 17h à 18h (partie 1, gratuit, [don attendu](/#anchor-price))
- Ce jeudi 28 mai 2020 de 17h à 18h (partie 2, gratuit, [don attendu](/#anchor-price))
- Ce jeudi 11 juin 2020 de 17h à 18h (partie 3, gratuit, [don attendu](/#anchor-price))

Dans le salon [EducodeTV](https://bbb-1.wsweet.cloud/b/nic-7vu-ygq) (qui ouvrira 30 minutes avant le cours).
---
description_long:

Cet atelier, animé par [Marie Duflot-Kremer](/intervenants/marie-duflot-kremer/), regroupe deux webinaires :

**Un peu d'informagie à faire à la maison :**

Nous vous proposons une petite parenthèse mêlant informatique et magie.
Non il n'y a rien de magique dans l'informatique, l'ordinateur n'a pas sa volonté propre et tout n'est pas "trop compliqué".  Nous vous proposons dans ce webinaire de découvrir des concepts informatiques et de les mettre en pratique avec des objets du quotidien, de manière amusante et apparemment magique. Alors venez vous amuser, réfléchir, comprendre et repartez avec quelques tours (informatiques) dans votre sac. Le premier tour de magie est compréhensible et faisable dès 5-6 ans.

**Jouons avec l'intelligence artificielle à la maison :**

Une nouvelle idée d'activité ludique et facilement reproductible avec des enfants à la maison ou ailleurs : parler d'intelligence artificielle et plus particulièrement d'apprentissage… avec des gobelets et des jetons. Venez découvrir comment une machine apparemment toute simple peut apprendre et devenir infaillible à un simple jeu de stratégie, et repartez avec une activité clé en main à reproduire avec vos amis/enfants/élèves.

** Sage comme une image ? **
En informatique toutes les informations sont codées en binaire, même les images. Dans cette activité, vous découvrirez comment on peut se transmettre une image et reconstituerez vous-mêmes une image à partir d'un codage qui permet de les compresser.

Indication : se munir de papier quadrillé et d'un crayon ou stylo noir.

**S'inscrire aux webinaires**

Ces deux webinaires sont accessibles aux personnes inscrites dans ce [formulaire](https://framaforms.org/educodeschool-inscription-aux-formations-en-ligne-1588750555).

**Liens**

* La page du site de [Marie Duflot-Kremer](/intervenants/marie-duflot-kremer/) sur laquelle elle propose quelques [Activités de médiation](https://members.loria.fr/MDuflot/files/med/index.html).
* Sur la page “[Enseigner et apprendre les sciences informatiques à l’école](https://interstices.info/enseigner-et-apprendre-les-sciences-informatiques-a-lecole/)” du site d'Interstices, vous trouverez la version traduite en français de l'incontournable [Computer Science Unplugged](https://interstices.info/wp-content/uploads/2018/01/csunplugged2014-fr-comp.pdf) [PDF - fr].

