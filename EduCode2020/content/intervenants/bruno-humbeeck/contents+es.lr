_model: speaker
---
title: Bruno Humbeeck
---
country: Belgium
---
description:

### Réinventer l'école par Bruno Humbeeck

Réinventer l’école en donnant au numérique une place… pas toute la place…

L’enseignement numérique, pendant cette période de confinement, a démontré qu’il était une nécessité. Il a aussi, dans le même temps, indiqué toutes ses limites. Indépendamment de la problématique de la fracture numérique qui a sauté aux yeux de tous avec une brutalité inédite, le rôle du présentiel et l’importance du groupe-classe dans la dynamique enseignante se sont révélés avec force.

N’est-ce pas le bon moment pour réinventer l’école et réaliser, à travers des méthodologies proches de celles de la “classe inversée”, le subtil mélange entre l’enseignement simultané, l’enseignement mutuel et l’enseignement différencié que rêvent tous ceux qui pensent que l’école doit toujours veiller à se donner les moyens de s’adresser à tous, tout en se préoccupant de la manière singulière dont chacun se développe ?

Des classes-bulles qui réduisent la taille des groupes… des capsules numériques de dix minutes pour expliquer la matière et permettre aux élèves d’y revenir tant qu’ils en éprouvent le besoin… un présentiel limité dans sa quantité mais gagnant en qualité… des enseignants valorisés dans le double rôle qu’ils se donnent de favoriser, au sein de leurs classes, les interactions apprenantes et de s’offrir en supports d’apprentissage… une école qui se définit d’abord et prioritairement par la qualité des liens qu’elle entretient avec chaque élève…

Autant d’innovations balbutiantes, mises en place dans l‘urgence, le plus souvent bricolées parce qu’il fallait en priorité faire face à des mesures sanitaires… mais aussi des opportunités de réfléchir à des manières nouvelles de faire la classe en s’autorisant à réinventer l’école…
---
name: Bruno Humbeeck
